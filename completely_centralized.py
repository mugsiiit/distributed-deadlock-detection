#!/usr/bin/python3

'''
Implementation of completely centralized algorithm
'''

import threading
import time
from queue import Queue
from copy import deepcopy
from helper_cycle import *

channels={}

from collections import defaultdict

class Node(threading.Thread):
	'''
	Class corresponding to Node i.e. individual nodes
	Input :
		thread_id : id for thread
		requested_resources : list of resources requested
		allocated_resources : list of resources allocated
		leader_node : chosen leader node
		nodes : all nodes in the system
	'''
	def __init__(self, thread_id, requested_resources, allocated_resources,leader_node,nodes=[]):
		threading.Thread.__init__(self)
		self.thread_id = thread_id
		self.leader_node=leader_node
		self.requested_resources=requested_resources
		self.allocated_resources=allocated_resources
		self.nodes=deepcopy(nodes)
		self.nodes_copy=deepcopy(nodes) # Copy of nodes
		self.allocations={}
		self.all_resources=[]
		self.request_for=defaultdict(lambda:[]) # Requests

	def run(self):
		self.nodes.remove(self.thread_id) # Don't need to send to myself
		global channels
		if self.thread_id!=self.leader_node: # If not a leader node
			channels[(self.thread_id,self.leader_node)].put(("NODE_INFO",[self.requested_resources,self.allocated_resources])) # Send Node Local Info regarding Resources
			print("NODE",self.thread_id,":","SENT RESOURCE INFORMATION :-","REQUESTED RESOURCES :",*self.requested_resources,"ALLOCATED RESOURCES :",*self.allocated_resources)
			print("NODE",self.thread_id,":","TERMINATED")
			raise SystemExit()
		else:
			self.all_resources+=self.requested_resources+self.allocated_resources
			for res in self.allocated_resources:
				self.allocations[res]=self.thread_id
			for res in self.requested_resources:
				self.request_for[res].append(self.thread_id)
			while(True):
				if len(self.nodes)==0: # Got info from all Nodes
					print("NODE",self.thread_id,":","GATHERED INFORMATION FROM ALL NODES")
					self.all_resources=list(set(self.all_resources))
					for res in self.all_resources:
						if res not in self.allocations:
							self.allocations[res]=self.request_for[res][0] # Allocate resource if not allocated to anyone
							self.request_for[res].pop(0)
					edges=defaultdict(lambda:[])
					for res in self.request_for:
						for node in self.request_for[res]:
							edges[node].append(self.allocations[res]) # Edges from waiting-for-resource-node to that node to which resource is allocated
					deadlock,path=detect_cycle(self.nodes_copy,edges) # Detect deadlock
					if(deadlock): # Detected deadlock
						print("NODE",self.thread_id,":","FOUND DEADLOCK DUE TO CYCLE",path)
					else:
						print("NODE",self.thread_id,":","NO DEADLOCK FOUND")
					print("NODE",self.thread_id,":","TERMINATED")
					raise SystemExit()
				else:
					for tid in self.nodes: # Get messages from channels
						q=channels[(tid,self.thread_id)]
						if not q.empty():
							msg=q.get()[1]
							self.all_resources+=msg[0]+msg[1]
							for res in msg[1]:
								self.allocations[res]=tid
							for res in msg[0]:
								self.request_for[res].append(tid)
							self.nodes.remove(tid)

# PARSE_INPUT

threads=[]

from copy import deepcopy

no_nodes,no_resources,sleep_time=map(int,input().split(','))
resources=input().replace(' ','').split(',')


node_info={}
node_ids=[]

for node in range(no_nodes):
	[node_id,n_req_res,n_allocated_res]=input().replace(' ','').split(',')
	node_info[node_id]={}
	node_ids.append(node_id)
	node_info[node_id]['req']=input().replace(' ','').split(',')
	try:
		node_info[node_id]['req'].remove('')
	except:
		pass
	node_info[node_id]['alloc']=input().replace(' ','').split(',')
	try:
		node_info[node_id]['alloc'].remove('')
	except:
		pass

leader_node=input()

for node in node_ids:
	threads.append(Node(node,node_info[node]['req'],node_info[node]['alloc'],leader_node,deepcopy(node_ids)))

for i in node_ids:
	for j in node_ids:
		if i!=j:
			channels[(i,j)]=Queue()

for i in threads:
	i.start()

for i in threads:
	i.join()

print("DEADLOCK DETECTION OVER")
