#!/usr/bin/python3

'''
Implementation of hiearchial algorithm of Menasce Muntz
'''

import threading
import time
from queue import Queue
from copy import deepcopy
from helper_cycle import *

channels={}

from collections import defaultdict

class LeafNode(threading.Thread):
	'''
	Class corresponding to leaf-node i.e. individual nodes
	Input :
		thread_id : id for thread
		nodes : nodes know to the node
		edges : outgoing edges from current node
		parent : parent of current node if exists
	'''
	def __init__(self, thread_id, nodes, edges,parent=None):
		threading.Thread.__init__(self)
		self.thread_id = thread_id
		self.nodes = nodes
		self.edges = edges
		self.parent = parent

	def run(self):
		global channels
		channels[(self.thread_id,self.parent)].put(("LOCAL_WFG",[self.nodes,self.edges])) # Send local WFG to Parent Node
		print("NODE",self.thread_id,":","SENT WFG")
		print("NODE",self.thread_id,":","TERMINATED")
		raise SystemExit()

class NonLeafNode(threading.Thread):
	'''
	Class corresponding to non-leaf-node i.e. parent nodes
	Input :
		thread_id : id for thread
		children : children-nodes know to the node
		parent : parent of current node if exists
	'''
	def __init__(self, thread_id, children, parent=None):
		threading.Thread.__init__(self)
		self.thread_id = thread_id
		self.children = children
		self.parent = parent

	def run(self):
		self.nodes=[]
		self.edges=defaultdict(lambda:[])
		global channels
		while(True):
			if len(self.children)==0: # If received local WFG from all children nodes
				(cycle,path)=detect_cycle(self.nodes,self.edges) # Deadlock Check
				if cycle: # Deadlock detected
					print("NODE",self.thread_id,":","DETECTED DEADLOCK")
					if self.parent: # If parent exists
						channels[(self.thread_id,self.parent)].put(("DEADLOCK_DETECTED",self.thread_id)) # Send Deadlock detected to parent, so he can terminate
					print("NODE",self.thread_id,":","TERMINATED") # We can terminate now
					raise SystemExit()
				else:
					print("NODE",self.thread_id,":","NO DETECTED DEADLOCK") # No Deadlock detected
					if self.parent: # If parent exists
						channels[(self.thread_id,self.parent)].put(("LOCAL_WFG",[self.nodes,self.edges])) # Send local WFG to parent
					print("NODE",self.thread_id,":","TERMINATED") # Root node so Terminate
					raise SystemExit()
			else:
				for node in self.children: # Check channel of children for messages
					q=channels[(node,self.thread_id)]
					if not q.empty():
						msg=q.get()
						if msg[0]=="LOCAL_WFG": # Get local WFG from child
							self.nodes+=msg[1][0] # Update nodes in local WFG
							for edge in msg[1][1]:
								self.edges[edge]+=msg[1][1][edge] # Update edges in local WFG
						else:
							print("NODE",self.thread_id,":","DEADLOCK MESSAGE FROM CHILD") # Child detected deadlock
							if self.parent:
								channels[(self.thread_id,self.parent)].put(("DEADLOCK_DETECTED",self.thread_id))
							print("NODE",self.thread_id,":","TERMINATED")
							raise SystemExit()
						self.children.remove(node)

# PARSE_INPUT

no_nodes,no_sites=map(int,input().split(','))

node_info={}
node_ids=[]

site_info={}
site_ids=[]


for node in range(no_nodes):
	[node_id,site_id,n_out_edges,n_in_edges]=input().replace(' ','').split(',')
	node_info[node_id]={}
	node_ids.append(node_id)
	node_info[node_id]['site']=site_id
	node_info[node_id]['out']=input().replace(' ','').split(',')
	try:
		node_info[node_id]['out'].remove('')
	except:
		pass
	node_info[node_id]['in']=input().replace(' ','').split(',')
	try:
		node_info[node_id]['in'].remove('')
	except:
		pass

for site in range(no_sites):
	[site_id,n_nodes_site]=input().replace(' ','').split(',')
	site_info[site_id]={}
	site_ids.append(site_id)
	site_info[site_id]['nodes']=input().replace(' ','').split(',')
	try:
		site_info[site_id]['nodes'].remove('')
	except:
		pass


[leader_node,leader_site]=input().split(',')

type_node=defaultdict(lambda:'')
prop_nodes=defaultdict(lambda:defaultdict(lambda:None))

for i in node_ids:
	prop_nodes[i]['nodes']=[i]
	prop_nodes[i]['edges']={i:node_info[i]['out']}

for i in node_ids:
	type_node[i]='LeafNode'

i=0
while(i+1<len(node_ids)):
	n1=node_ids[i]
	n2=node_ids[i+1]
	n3=n1+'_'+n2
	node_ids.append(n3)
	prop_nodes[n1]['parent']=n3
	prop_nodes[n2]['parent']=n3
	prop_nodes[n3]['children']=[n1,n2]
	channels[(n1,n3)]=Queue()
	channels[(n2,n3)]=Queue()
	type_node[n3]='NonLeafNode'
	i+=2


threads=[]

for i in node_ids:
	if type_node[i]=='LeafNode':
		threads.append(LeafNode(i,prop_nodes[i]['nodes'],prop_nodes[i]['edges'],prop_nodes[i]['parent']))
	else:
		if prop_nodes[i]['parent']:
			threads.append(NonLeafNode(i,prop_nodes[i]['children'],prop_nodes[i]['parent']))
		else:
			threads.append(NonLeafNode(i,prop_nodes[i]['children']))

for i in threads:
	i.start()

for i in threads:
	i.join()

print("DEADLOCK DETECTION OVER")