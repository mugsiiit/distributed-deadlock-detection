#!/usr/bin/python3

'''
Implementation of Obermack's algorithm
'''

import threading
import time
from queue import Queue
from copy import deepcopy
from helper_cycle import *

channels={}

class SiteNode(threading.Thread):
	'''
	Class corresponding to Node i.e. individual nodes
	Input :
		thread_id : id for thread
		site : Site corresponding to current node
		incoming_nodes : list of incoming_edges (site_id,node_id)s
		outgoing_nodes : list of outgoing_edges (site_id,node_id)s
	'''
	def __init__(self, thread_id, incoming_nodes, outgoing_nodes, site,start_node=False):
		threading.Thread.__init__(self)
		self.thread_id = thread_id
		self.incoming_nodes = incoming_nodes
		self.outgoing_nodes = outgoing_nodes
		self.site = site
		self.start_node=start_node

	def run(self):
		global channels
		channels[self.thread_id+'_'+self.site].put(("NODE_INFO",[self.start_node,self.incoming_nodes,self.outgoing_nodes])) # Push local WFG Info to X-Node
		print("NODE",self.thread_id,":","TERMINATED")
		raise SystemExit()

class SiteNodeX(threading.Thread):
	'''
	Class corresponding to X-Node of a Site
	Input :
		thread_id : id for thread
		site_nodes : nodes in site
		rounds : number of rounds to run
	'''
	def __init__(self, thread_id, site_nodes,rounds):
		threading.Thread.__init__(self)
		self.thread_id = thread_id
		self.site_nodes = site_nodes
		self.start_node = False
		self.incoming_nodes=[]
		self.outgoing_nodes=[]
		self.current_cycle=""
		self.rounds=rounds
		self.site_nodes_copy=deepcopy(site_nodes)

	def run(self):
		global channels
		self.edges={}
		for tid in self.site_nodes:
			self.edges[tid]=[]
		self.edges[self.thread_id]=[]

		while(True):
			if len(self.site_nodes)==0: # Got Local WFG Info
				print("NODE",self.thread_id,":","GATHERED INFORMATION ABOUT THE SITE")
				internal_cycle=detect_cycle(self.site_nodes_copy,self.edges) # Detect internal Cycle in System
				if internal_cycle[0]:
					print("NODE",self.thread_id,":","DETECTED INTERNAL CYCLE IN SITE :",internal_cycle[1])
					for site in self.outgoing_nodes:
						channels[self.thread_id+'_'+site].put(("DEADLOCK_DETECT"))
					print("NODE",self.thread_id,":","TERMINATED")
					raise SystemExit()
				possible_cycle=detect_cycle(self.site_nodes_copy+[self.thread_id],self.edges) # Detect possible cycle in System
				if not possible_cycle[0]: # If no possibility just exit
					print("NODE",self.thread_id,":","NO POSSIBILITY OF CYCLE IN SITE")
					for site in self.outgoing_nodes:
						channels[self.thread_id+'_'+site].put(("SITE_TERM"))
					print("NODE",self.thread_id,":","TERMINATED")
					raise SystemExit()
				break
			else:
				for tid in self.site_nodes: # Get messages from children
					q=channels[tid+'_'+self.thread_id]
					if not q.empty():
						msg=q.get()
						for inc_node in msg[1][1]:
							if inc_node[0] != self.thread_id:
								self.incoming_nodes.append(inc_node[0])
								self.edges[self.thread_id].append(tid)
						for out_node in msg[1][2]:
							if out_node[0] != self.thread_id:
								self.outgoing_nodes.append(out_node[0])
								self.edges[tid].append(self.thread_id)
							else:
								self.edges[tid].append(out_node[1])
						self.site_nodes.remove(tid)
		possible_cycle=detect_cycle(self.site_nodes_copy+[self.thread_id],self.edges)[1].replace(self.thread_id,'X') # Possible cycle of node
		for site in self.outgoing_nodes:
			channels[self.thread_id+'_'+site].put(("PATH_PUSH",possible_cycle))
		for rnd in range(self.rounds): # Run rounds
			print("NODE",self.thread_id,":","BEGINNING ROUND : ",rnd+1)
			for inc_node in self.incoming_nodes:
				q=channels[inc_node+'_'+self.thread_id]
				if not q.empty():
					msg=q.get()
					if msg[0]=="DEADLOCK_DETECT": # Someone else detected deadlock, no need to continue for us
						print("NODE",self.thread_id,":",inc_node," HAS DETECTED DEADLOCK")
						print("NODE",self.thread_id,":", "DETECTED DEADLOCK, FWD MESSAGE")
						for tid_o in self.outgoing_nodes:
							channels[(self.thread_id,tid_o)].put(("DEADLOCK_DETECT",self.thread_id))
						print("NODE",self.thread_id,":","TERMINATED")
						raise SystemExit()
					elif msg[0]=="NODE_TERM": # Some site-node terminated
						self.incoming_nodes.remove(inc_node)
						print("NODE",self.thread_id,":",inc_node," HAS TERMINATED RECV.")
					else:
						print("NODE",self.thread_id,":","RECEIVED PATH LIST FROM ",inc_node,msg[1]) # Got path pushed from another node
						for tid in self.site_nodes_copy:
							if tid in msg[1]: # Detected deadlock by me
								print("NODE",self.thread_id,":", "DETECTED DEADLOCK, FWDING MESSAGE")
								for tid_o in self.outgoing_nodes:
									channels[self.thread_id+'_'+tid_o].put(("DEADLOCK_DETECT",self.thread_id)) # Send deadlock detection message
								print("NODE",self.thread_id,":","TERMINATED")
								raise SystemExit()
						else:
							for tid_o in self.outgoing_nodes:
								channels[self.thread_id+'_'+tid_o].put(("PATH_PUSH",(msg[1][:-1]+possible_cycle[1:]).replace('-X-','-').replace('--','-'))) # Join my path and send
		print("NODE",self.thread_id,":","TERMINATED")
		raise SystemExit()

# PARSE_INPUT

no_nodes,no_sites=map(int,input().split(','))

node_info={}
node_ids=[]

site_info={}
site_ids=[]


for node in range(no_nodes):
	[node_id,site_id,n_out_edges,n_in_edges]=input().replace(' ','').split(',')
	node_info[node_id]={}
	node_ids.append(node_id)
	node_info[node_id]['site']=site_id
	node_info[node_id]['out']=input().replace(' ','').split(',')
	try:
		node_info[node_id]['out'].remove('')
	except:
		pass
	node_info[node_id]['in']=input().replace(' ','').split(',')
	try:
		node_info[node_id]['in'].remove('')
	except:
		pass

for site in range(no_sites):
	[site_id,n_nodes_site]=input().replace(' ','').split(',')
	site_info[site_id]={}
	site_ids.append(site_id)
	site_info[site_id]['nodes']=input().replace(' ','').split(',')
	try:
		site_info[site_id]['nodes'].remove('')
	except:
		pass


[leader_node,leader_site]=input().split(',')

for node in node_ids:
	new_in_edges=[]
	for in_edge in node_info[node]['in']:
		new_in_edges.append((node_info[in_edge]['site'],in_edge))
	node_info[node]['in']=new_in_edges

	new_out_edges=[]
	for out_edge in node_info[node]['out']:
		new_out_edges.append((node_info[out_edge]['site'],out_edge))
	node_info[node]['out']=new_out_edges

for i in node_ids:
	channels[i+'_'+node_info[i]['site']]=Queue()
	channels[node_info[i]['site']+'_'+i]=Queue()

for i in node_ids:
	for j in node_ids:
		if i!=j:
			channels[i+'_'+j]=Queue()

for i in site_ids:
	for j in site_ids:
		if i!=j:
			channels[i+'_'+j]=Queue()

threads = []

for i in node_ids:
	threads.append(SiteNode(i,node_info[i]['in'],node_info[i]['out'],node_info[i]['site']))
# threads.append(SiteNode('A', [('4','L')], [('1','B')], '1'))

for i in site_ids:
	threads.append(SiteNodeX(i,site_info[i]['nodes'],len(node_ids)))
# threads.append(SiteNodeX('1', ['A','B','C'],12))

for i in threads:
	i.start()

for i in threads:
	i.join()

print("DEADLOCK DETECTION OVER")
