# Path-Pushing-Algorithms
+ Implementation of Path Pushing Algorithms in Distributed Systems
+ Refer to [InputFormat.md](./InputFormat.md) for creating sample inputs

# Running sample inputs
+ Goldman :
  1. python3 goldman.py < Inputs/4-cycle-A.txt 
  2. python3 goldman.py < Inputs/4-no-cycle-A.txt
  3. python3 goldman.py < Inputs/12-cycle-A.txt
+ Menasce-Muntz(Hiearchial) :
  1. python3 menasce-muntz_hiearchial.py < Inputs/4-cycle-A.txt 
  2. python3 menasce-muntz_hiearchial.py < Inputs/4-no-cycle-A.txt
  3. python3 menasce-muntz_hiearchial.py < Inputs/12-cycle-A.txt
+ Completely Centralized :
  1. python3 completely_centralized.py < Inputs/4-cycle-B-no-sleep.txt
  2. python3 completely_centralized.py < Inputs/4-cycle-B-5s-sleep.txt
  3. python3 completely_centralized.py < Inputs/4-no-cycle-B-no-sleep.txt
+ Ho-Ramamoorthy(One Phased) :
  1. python3 ho-ramamoorthy_one_phase.py < Inputs/4-cycle-B-no-sleep.txt
  2. python3 ho-ramamoorthy_one_phase.py < Inputs/4-cycle-B-5s-sleep.txt
  3. python3 ho-ramamoorthy_one_phase.py < Inputs/4-no-cycle-B-no-sleep.txt
+ Obermack :
  1. python3 obermacks.py < Inputs/4-cycle-A.txt 
  2. python3 obermacks.py < Inputs/4-no-cycle-A.txt
  3. python3 obermacks.py < Inputs/12-cycle-A.txt
