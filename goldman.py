#!/usr/bin/python3

'''
Implementation of Goldman Algo
'''

import threading
import time
from queue import Queue

channels={}

class Node(threading.Thread):
	'''
	Class corresponding to Node i.e. individual nodes
	Input :
		thread_id : id for thread
		incoming_nodes : list of incoming nodes
		outgoing_nodes : list of outgoing nodes
		start_node : boolean if start variable
	'''
	def __init__(self, thread_id, incoming_nodes=[], outgoing_nodes=[],start_node=False):
		threading.Thread.__init__(self)
		self.thread_id = thread_id
		self.incoming_nodes = incoming_nodes
		self.outgoing_nodes = outgoing_nodes
		self.start_node = start_node

	def run(self):
		global channels
		if self.start_node: # If starting node
			for tid in self.outgoing_nodes:
				channels[(self.thread_id,tid)].put(("PATH_PUSH",[self.thread_id,tid])) # Send [cur_node,next_node] to next_node
		while(True):
			if len(self.incoming_nodes)==0: # Received messages from all input nodes
				for tid in self.outgoing_nodes:
					channels[(self.thread_id,tid)].put(("NODE_TERM",self.thread_id)) # Node terminating message send
				print("NODE",self.thread_id,":","TERMINATED") # Terminate Node
				raise SystemExit()
			if len(self.outgoing_nodes)==0: # Received messages from all input nodes
				for tid in self.incoming_nodes:
					channels[(self.thread_id,tid)].put(("NODE_TERM",self.thread_id)) # Node terminating message send
				print("NODE",self.thread_id,":","TERMINATED") # Terminate Node
				raise SystemExit()
			for tid in self.incoming_nodes+self.outgoing_nodes: # Check channels for messages
				q=channels[(tid,self.thread_id)] # Channel corresponding to incoming nodes
				if not q.empty():
					msg=q.get()
					if msg[0]=="NODE_TERM":
						try:
							self.incoming_nodes.remove(tid) # Remove node if terminated
							print("NODE",self.thread_id,":",tid," HAS TERMINATED RECV.")
						except:
							self.outgoing_nodes.remove(tid)
							print("NODE",self.thread_id,":",tid," HAS TERMINATED RECV.")
					elif msg[0]=="DEADLOCK_DETECT":
						self.incoming_nodes.remove(tid) # If deadlock detected, forward and exit
						print("NODE",self.thread_id,":",tid," HAS DETECTED DEADLOCK")
						print("NODE",self.thread_id,":", "DETECTED DEADLOCK, FWD MESSAGE")
						for tid_o in self.outgoing_nodes:
							channels[(self.thread_id,tid_o)].put(("DEADLOCK_DETECT",self.thread_id))
						print("NODE",self.thread_id,":","TERMINATED")
						raise SystemExit()
					else:
						print("NODE",self.thread_id,":","RECEIVED PATH LIST FROM ",tid,msg[1]) # Get path list
						deadlock=False
						for tid_o in self.outgoing_nodes:
							if tid_o in msg[1]:
								deadlock=True
								break
						if deadlock:
							print("NODE",self.thread_id,":", "DETECTED DEADLOCK, FWDING MESSAGE") # Detect deadlock
							for tid_o in self.outgoing_nodes:
								channels[(self.thread_id,tid_o)].put(("DEADLOCK_DETECT",self.thread_id))
							print("NODE",self.thread_id,":","TERMINATED")
							raise SystemExit()
						else:
							for tid_o in self.outgoing_nodes:
								channels[(self.thread_id,tid_o)].put(("PATH_PUSH",msg[1]+[tid_o])) # Append and forward path

# PARSE_INPUT

threads = []

no_nodes,no_sites=map(int,input().split(','))

node_info={}
node_ids=[]

site_info={}
site_ids=[]


for node in range(no_nodes):
	[node_id,site_id,n_out_edges,n_in_edges]=input().replace(' ','').split(',')
	node_info[node_id]={}
	node_ids.append(node_id)
	node_info[node_id]['site']=site_id
	node_info[node_id]['out']=input().replace(' ','').split(',')
	try:
		node_info[node_id]['out'].remove('')
	except:
		pass
	node_info[node_id]['in']=input().replace(' ','').split(',')
	try:
		node_info[node_id]['in'].remove('')
	except:
		pass


for site in range(no_sites):
	[site_id,n_nodes_site]=input().replace(' ','').split(',')
	site_info[site_id]={}
	site_ids.append(site_id)
	site_info[site_id]['nodes']=input().replace(' ','').split(',')
	try:
		site_info[site_id]['nodes'].remove('')
	except:
		pass

[leader_node,leader_site]=input().split(',')

for node in node_ids:
	threads.append(Node(node,node_info[node]['in'],node_info[node]['out'],node==leader_node))

for i in node_ids:
	for j in node_ids:
		if i!=j:
			channels[(i,j)]=Queue()

for i in threads:
	i.start()

for i in threads:
	i.join()

print("DEADLOCK DETECTION OVER")
