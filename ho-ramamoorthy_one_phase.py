#!/usr/bin/python3

'''
Implementation of one-phase algorithm of Ho-Ramamoorthy
'''

import threading
import time
from queue import Queue
from copy import deepcopy
from helper_cycle import *

channels={}

from collections import defaultdict

class ReqMsgThread(threading.Thread):
	'''
	Class corresponding to Resource request
	Input :
		channel : channel to which message is sent
		msg : msg being sent
		sleep_time : time to sleep
	'''
	def __init__(self, channel,msg,sleep_time):
		threading.Thread.__init__(self)
		self.channel=channel
		self.msg=msg
		self.sleep_time=sleep_time

	def run(self):
		self.channel.put(self.msg) # Send message
		time.sleep(self.sleep_time) # Sleep
		raise SystemExit()

class Resource(threading.Thread):
	'''
	Class corresponding to each Resource
	Input :
		thread_id : thread id
		nodes : all nodes in system
	'''
	def __init__(self, thread_id, nodes):
		threading.Thread.__init__(self)
		self.thread_id = thread_id
		self.nodes = nodes
		self.allocated = None # Not allocated to any node initially
		self.in_queue = [] # Request Queue
	def run(self):
		while(True):
			for tid in self.nodes:
				q=channels[tid,self.thread_id]
				if not q.empty():
					msg=q.get()
					if msg[0]=="RESOURCE_REQUEST": # Request to allocate resource
						if not self.allocated:
							self.allocated=tid # Allot if not alloted to anyone
							channels[self.thread_id,tid].put(("RESOURCE_ALLOCATED",self.thread_id))
						else:
							self.in_queue.append(tid) # Put in queue if not allocated
							channels[self.thread_id,tid].put(("IN_QUEUE",self.thread_id))
					else:
						print("RESOURCE",self.thread_id,":","DEADLOCK DETECTION OVER, EXITING THREAD") # Exit if nodes are done with deadlock detection
						raise SystemExit()

class Node(threading.Thread):
	'''
	Class corresponding to Node i.e. individual nodes
	Input :
		thread_id : id for thread
		requested_resources : list of resources requested
		leader_node : chosen leader node
		sleep_time : time to sleep after each request
		nodes : all nodes in the system
	'''
	def __init__(self, thread_id, requested_resources,leader_node,sleep_time,nodes=[]):
		threading.Thread.__init__(self)
		self.thread_id = thread_id
		self.leader_node=leader_node
		self.requested_resources=[]
		self.requested_resources_copy=deepcopy(requested_resources)
		self.allocated_resources=[]
		self.nodes=deepcopy(nodes)
		self.nodes_copy=deepcopy(nodes)
		self.allocations={}
		self.all_resources=[]
		self.request_for=defaultdict(lambda:[])
		self.sleep_time=sleep_time

	def run(self):
		global channels
		for res in self.requested_resources_copy:
			tmp_thread=ReqMsgThread(channels[self.thread_id,res],("RESOURCE_REQUEST",self.thread_id),self.sleep_time) # Send requests
			tmp_thread.start()
			tmp_thread.join() # Wait for them to be done with
		print("NODE",self.thread_id,":","RESOURCES REQUESTED")
		while(len(self.requested_resources_copy)): # Get messages from Nodes regarding allocation status
			for res in self.requested_resources_copy:
				q=channels[res,self.thread_id]
				if not q.empty():
					msg=q.get()
					if msg[0]=="RESOURCE_ALLOCATED":
						print("NODE",self.thread_id,":","RESOURCE",res,"ALLOCATED")
						self.allocated_resources.append(res)
					else:
						print("NODE",self.thread_id,":","IN QUEUE OF","RESOURCE",res)
						self.requested_resources.append(res)
					self.requested_resources_copy.remove(res)

		self.nodes.remove(self.thread_id)
		if self.thread_id!=self.leader_node:
			channels[(self.thread_id,self.leader_node)].put(("NODE_INFO",[self.requested_resources,self.allocated_resources])) # Send info to leader node
			print("NODE",self.thread_id,":","SENT RESOURCE INFORMATION :-","REQUESTED RESOURCES :",*self.requested_resources,"ALLOCATED RESOURCES :",*self.allocated_resources)
			print("NODE",self.thread_id,":","TERMINATED")
			raise SystemExit()
		else:
			self.all_resources+=self.requested_resources+self.allocated_resources
			for res in self.allocated_resources:
				self.allocations[res]=self.thread_id
			for res in self.requested_resources:
				self.request_for[res].append(self.thread_id)
			while(True):
				if len(self.nodes)==0: # Got infor from all non-leader nodes
					print("NODE",self.thread_id,":","GATHERED INFORMATION FROM ALL SITES")
					self.all_resources=list(set(self.all_resources))
					for res in self.all_resources:
						if res not in self.allocations:
							self.allocations[res]=self.request_for[res][0]
							self.request_for[res].pop(0)
					edges=defaultdict(lambda:[])
					for res in self.request_for:
						for node in self.request_for[res]:
							edges[node].append(self.allocations[res]) # Construct edges
					deadlock,path=detect_cycle(self.nodes_copy,edges) # Detect cycle
					if(deadlock):
						print("NODE",self.thread_id,":","FOUND DEADLOCK DUE TO CYCLE",path) # Report deadlock found or node
					else:
						print("NODE",self.thread_id,":","NO DEADLOCK FOUND")
					print("NODE",self.thread_id,":","TERMINATED")
					for res in self.all_resources:
						channels[(self.thread_id,res)].put(("TERM_NODE",self.thread_id)) # Resource nodes can terminate now
					raise SystemExit()
				else:
					for tid in self.nodes: # Get messsages from non-leader nodes
						q=channels[(tid,self.thread_id)]
						if not q.empty():
							msg=q.get()[1]
							self.all_resources+=msg[0]+msg[1]
							for res in msg[1]:
								self.allocations[res]=tid
							for res in msg[0]:
								self.request_for[res].append(tid)
							self.nodes.remove(tid)

threads=[]

from copy import deepcopy

no_nodes,no_resources,sleep_time=map(int,input().split(','))
resources=input().replace(' ','').split(',')


node_info={}
node_ids=[]

for node in range(no_nodes):
	[node_id,n_req_res,n_allocated_res]=input().replace(' ','').split(',')
	node_info[node_id]={}
	node_ids.append(node_id)
	node_info[node_id]['req']=input().replace(' ','').split(',')
	try:
		node_info[node_id]['req'].remove('')
	except:
		pass
	node_info[node_id]['alloc']=input().replace(' ','').split(',')
	try:
		node_info[node_id]['alloc'].remove('')
	except:
		pass

leader_node=input()

for res_id in resources:
	threads.append(Resource(res_id,deepcopy(node_ids)))

for node in node_ids:
	threads.append(Node(node,node_info[node]['alloc']+node_info[node]['req'],leader_node,sleep_time,deepcopy(node_ids)))

for i in node_ids:
	for j in node_ids:
		if i!=j:
			channels[(i,j)]=Queue()

for i in node_ids:
	for j in node_ids:
		if i!=j:
			channels[(i,j)]=Queue()

for i in node_ids:
	for j in resources:
			channels[(i,j)]=Queue()
			channels[(j,i)]=Queue()

for i in threads:
	i.start()

for i in threads:
	i.join()

print("DEADLOCK DETECTION OVER")