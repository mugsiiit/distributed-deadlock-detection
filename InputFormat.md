1. For goldman,obermacks,menasce
	+ Line 1 : Number of Nodes, Number of Sites
	+ ** For each Node : **
	   1. Node-id,Site,No-of-edges-outgoing,No-of-edges-incoming
	   2. Ids-of-outgoing-edges-seperated-by-comma
	   3. Ids-of-incoming-edges-seperated-by-comma
	+ ** For each Site : **
	   1. Site-id,Number-of-nodes-in-site
	   2. Nodes-in-site-seperated-by-comma
	+ Leader-Node,Leader-Site(Gibberish-if-not-req)
2. For ho-ramamoorthy
	+ Line 1 : Number-of-Nodes, Number-of-Resources, Sleep-Time-Between-Requests
	+ Line 2 : Resources-ids-seperated-by-comma
	+ ** For each Node : **
	   1. Node-id,No-of-resources-req,No-of-resources-allocated
	   2. Ids-of-requested-resources-seperated-by-comma
	   3. Ids-of-allocated-resources-seperated-by-comma
	+ Leader-Node(Gibberish-if-not-req)
