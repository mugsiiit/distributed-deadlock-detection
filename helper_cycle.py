#!/usr/bin/python3

from collections import defaultdict

def dfs_cycle(u,p,color,mark,par,cyclenumber,edges,nodes):
	'''
		@dfs_cycle : Detect if cycle exists using dfs - Recursive
		u - current_node
		p - parent
	'''
	if color[u]==2:
		return cyclenumber,mark,par
 
	if color[u] == 1: 
		cyclenumber+=1
		cur=p
		mark[cur]=cyclenumber

		while cur!=u:
			cur=par[cur]
			mark[cur]=cyclenumber
		return cyclenumber,mark,par
	par[u] = p 

	color[u] = 1
	for v in edges[u]:
		if v in nodes:
			if v==par[u]:
				continue
			cyclenumber,mark,par=dfs_cycle(v, u, color, mark, par,cyclenumber,edges,nodes) 
	color[u] = 2
	return cyclenumber,mark,par

def getCycle(edges, mark,cycles,par,s_node):
	'''
		@getCycle : Get cycles from output of @dfs_cycle
	'''
	for i in mark:
		if mark[i]!=0:
			cycles[mark[i]].append(i)
	rev_par={}
	for i in par:
		rev_par[par[i]]=i
	cycle_path=''
	s_node=''
	while(True):
		try:
			s_node=rev_par[s_node]
			cycle_path+='-'+s_node
		except:
			break
	return cycle_path[1:]+'-'+rev_par['']

def detect_cycle(nodes,edges):
	'''
		@detect_cycle : Check and find the cycles for given graph
	'''
	shortest_cycle=""
	cycle=False
	for node in nodes:
		color=defaultdict(lambda:0)
		par=defaultdict(lambda:0)
		mark=defaultdict(lambda:0)
		cycles=defaultdict(lambda:[])
		cyclenumber = 0
		n_nodes=len(nodes)
		cyclenumber,mark,par=dfs_cycle(node, '', color, mark, par,cyclenumber,edges, nodes)
		# print(mark)
		# print(par)
		cycle=False
		path=''
		if cyclenumber:
			temp_pth=getCycle(edges,mark,cycles,par,node)
			if not cycle:
				path=temp_pth
				cycle=True
			else:
				if len(temp_pth.split('-'))<len(path.split('-')):
					path=temp_pth
	return (cycle,path)